<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Graph
 *
 * @author fabien.sanchez
 */
class Graph
{

    private $height;
    private $width;
    private $data;
    private $color;

    public function __construct($width, $height)
    {
        $this->width = $width;
        $this->height = $height;
    }

    public function setData($data, Color $color)
    {
        $this->data = $data;
        $this->color = $color;
        return $this;
    }

    private function normalizData()
    {
        $newData = [];
        $max = max($this->data);
        foreach ($this->data as $key => $value) {
            $newData[$key] = round($value * $this->height / $max);
        }
        return $newData;
    }

    private function gdRender()
    {
        $image = imagecreatetruecolor($this->width, $this->height);
        $bg = imagecolorallocate($image, 255, 255, 255);
        imagefill($image, 0, 0, $bg);

        if ($this->width < count($this->data)) {
            throw new Exception("Impossible d'afficher correctement le graph!");
        }

        $data = $this->normalizData();
        $index = 0;
        $rgb = $this->color->getRGB();
        $color = imagecolorallocate($image, $rgb[0], $rgb[1], $rgb[2]);
        foreach ($data as $value) {
            $x1 = round($index * $this->width / count($this->data));
            $y1 = $this->height - 1 - $value;
            $index++;
            $x2 = round($index * $this->width / count($this->data)) - 1;
            $y2 = $this->height - 1;
            imagefilledrectangle($image, $x1, $y1, $x2, $y2, $color);
        }

        return $image;
    }

    public function saveToPng($filename)
    {
        $g = $this->gdRender();
        imagepng($g, $filename, 0);
        imagedestroy($g);
    }

}

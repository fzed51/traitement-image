<?php

/**
 * Description of Image
 *
 * @author fabien.sanchez
 */
class Image
{

    private $width = 0;
    private $height = 0;
    private $pixels = [];

    static public function loadFromJpg($filename)
    {
        $image = new self();
        list($width, $height, $type, $attr) = getimagesize($filename);
        $jpg = imagecreatefromjpeg($filename);

        $image->width = $width;
        $image->height = $height;
        for ($y = 0; $y < $height; $y++) {
            for ($x = 0; $x < $width; $x++) {
                $rgb = imagecolorat($jpg, $x, $y);
                $red = ($rgb >> 16) & 0xFF;
                $green = ($rgb >> 8) & 0xFF;
                $blue = $rgb & 0xFF;
                $image->pixels[$image->getIndex($x, $y)] = new Color($red, $green, $blue);
            }
        }

        imagedestroy($jpg);

        return $image;
    }

    public function __clone()
    {
        foreach ($this->pixels as &$pixel) {
            $pixel = clone $pixel;
        }
    }

    private function getIndex($x, $y)
    {
        return ($y ) * $this->width + ($x );
    }

    private function getCoord($index)
    {
        return [($index % $this->width), floor($index / $this->width)];
    }

    public function getPixel($x, $y)
    {
        return $this->pixels[$this->getIndex($x, $y)];
    }

    public function setPixel($x, $y, Color $color)
    {
        $this->pixels[$this->getIndex($x, $y)] = $color;
    }

    private function createImage()
    {
        $img = imagecreatetruecolor($this->width, $this->height);
        for ($index = 0; $index < count($this->pixels); $index++) {
            list($red, $green, $blue) = $this->pixels[$index]->getRGB();
            list($x, $y) = $this->getCoord($index);
            $color = imagecolorallocate($img, $red, $green, $blue);
            imagesetpixel($img, $x, $y, $color);
        }
        return $img;
    }

    public function saveToJpg($path)
    {
        $img = $this->createImage();
        imagejpeg($img, $path, 85);
        imagedestroy($img);
    }

    public function saveToPng($path)
    {
        $img = $this->createImage();
        imagepng($img, $path, 0);
        imagedestroy($img);
    }

// ------------------------------------------------------------------------

    public function convertToGray()
    {
        $newImg = clone $this;
        foreach ($newImg->pixels as $index => $color) {
            $pixel = $newImg->pixels[$index];
            $gray = $pixel->getGray();
            $pixel->setGray($gray);
            $newImg->pixels[$index] = $pixel;
        }
        return $newImg;
    }

    public function convertToRed()
    {
        $newImg = clone $this;
        foreach ($newImg->pixels as $index => $color) {
            $pixel = $newImg->pixels[$index];
            $red = $pixel->getRed();
            $pixel->setGray($red);
            $newImg->pixels[$index] = $pixel;
        }
        return $newImg;
    }

    public function convertToGreen()
    {
        $newImg = clone $this;
        foreach ($newImg->pixels as $index => $color) {
            $pixel = $newImg->pixels[$index];
            $green = $pixel->getGreen();
            $pixel->setGray($green);
            $newImg->pixels[$index] = $pixel;
        }
        return $newImg;
    }

    public function convertToBlue()
    {
        $newImg = clone $this;
        foreach ($newImg->pixels as $index => $color) {
            $pixel = $newImg->pixels[$index];
            $blue = $pixel->getBlue();
            $pixel->setGray($blue);
            $newImg->pixels[$index] = $pixel;
        }
        return $newImg;
    }

    public function getHistogram($colorSet = 7)
    {
        // Initialisation
        $histogram = [];
        for ($i = 0; $i < 256; $i++)
            $histogram[$i] = 0;
        // Analyse des donnée
        foreach ($this->pixels as $color) {
            if ($colorSet == 7) {
                $histogram[$color->getGray()] ++;
            } else {
                if (($colorSet & 1) == 1) {
                    $histogram[$color->getRed()] ++;
                }
                if (($colorSet & 2) == 2) {
                    $histogram[$color->getGreen()] ++;
                }
                if (($colorSet & 4) == 4) {
                    $histogram[$color->getBlue()] ++;
                }
            }
        }
        // Normalisation
        $max = max($histogram);
        for ($i = 0; $i < count($histogram); $i++) {
            $histogram[$i] = round($histogram[$i] * 100 / $max);
        }

        return $histogram;
    }

}

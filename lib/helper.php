<?php

function showImage($imageAvantPath, $imagesApres)
{
    ?>
    <!DOCTYPE HTML>
    <html>
        <head>
            <title>Traitement de l'image</title>
            <meta charset="utf-8" />
            <meta name="viewport" content="width=device-width, initial-scale=1" />
            <style>
                img{
                    margin: 1em;
                    box-shadow: 0 0 5px #888;
                }
            </style>
        </head>
        <body>
            <h2>
                Image avant
            </h2>
            <img src="<?= $imageAvantPath; ?>" alt="" />
            <h2>
                Image après
            </h2>
            <?php foreach ($imagesApres as $titre => $imageApresPath) : ?>
                <h3><?= $titre; ?></h3>
                <img src="<?= $imageApresPath; ?>" alt="" />
            <?php endforeach; ?>
            <script id="__bs_script__">
                    document.write("<script async src='http://HOST:3000/browser-sync/browser-sync-client.2.12.8.js'><\/script>".replace("HOST", location.hostname));
            </script>
        </body>
    </html>
    <?php
}

<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Color
 *
 * @author fabien.sanchez
 */
class Color
{

    private $red;
    private $green;
    private $blue;

    public function __construct($red, $green = null, $blue = null)
    {
        $this->red = $red;
        if (is_null($green) || is_null($blue)) {
            $this->green = $red;
            $this->blue = $red;
        } else {
            $this->green = $green;
            $this->blue = $blue;
        }
    }

    public function setGray($gray)
    {
        $this->red = $gray;
        $this->green = $gray;
        $this->blue = $gray;
    }

    public function setRGB($red, $green, $blue)
    {
        $this->red = $red;
        $this->green = $green;
        $this->blue = $blue;
    }

    public function getGray()
    {
        return round((
                $this->red +
                $this->green +
                $this->blue
                ) / 3);
    }

    public function getRed()
    {
        return $this->red;
    }

    public function getGreen()
    {
        return $this->green;
    }

    public function getBlue()
    {
        return $this->blue;
    }

    public function getRGB()
    {
        return [
            $this->red,
            $this->green,
            $this->blue
        ];
    }

}

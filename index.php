<?php

$libs = glob('./lib/*.php');
foreach ($libs as $lib) {
    include $lib;
}


$avant = './image/avant/moniteur4in1.jpg';
$apresjpg = './image/apres/moniteur4in1.jpg';
$aprespng = './image/apres/moniteur4in1.png';
$apresVpng = './image/apres/moniteur4in1-vert.png';
$histoGpng = './image/apres/histogramG.png';
$histoRpng = './image/apres/histogramR.png';
$histoVpng = './image/apres/histogramV.png';
$histoBpng = './image/apres/histogramB.png';

//var_dump(7 & 2);die();

$img = Image::loadFromJpg($avant);

$img->convertToGray()
        ->saveToPng($aprespng);
$img->convertToBlue()
        ->saveToPng($apresVpng);

$img->saveToJpg($apresjpg);

(new Graph(256, 128))
        ->setData($img->getHistogram(), new Color(128, 128, 128))
        ->saveToPng($histoGpng);
(new Graph(256, 128))
        ->setData($img->getHistogram(1), new Color(255, 0, 0))
        ->saveToPng($histoRpng);
(new Graph(256, 128))
        ->setData($img->getHistogram(2), new Color(0, 255, 0))
        ->saveToPng($histoVpng);
(new Graph(256, 128))
        ->setData($img->getHistogram(4), new Color(0, 0, 255))
        ->saveToPng($histoBpng);

showImage($avant, [
    "jpg réécrit" => $apresjpg,
    "png gris" => $aprespng,
    "png vert" => $apresVpng,
    "Histogramme gris" => $histoGpng,
    "Histogramme rouge" => $histoRpng,
    "Histogramme vert" => $histoVpng,
    "Histogramme bleu" => $histoBpng
]);
